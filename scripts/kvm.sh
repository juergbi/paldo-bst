#!/bin/bash

set -e

bst build paldo.bst

ROOTDIR=`mktemp --directory --tmpdir="$PWD"`

bst checkout --hardlinks paldo.bst $ROOTDIR

kvm \
  -cpu host -smp 4,sockets=1 -m 4G -vga virtio \
  -kernel $ROOTDIR/usr/lib/kernel/image -append "rootfstype=9p rootflags=trans=virtio rw init=/bin/bash" \
  -fsdev local,id=root,path=/,security_model=none -device virtio-9p-pci,fsdev=root,mount_tag=/dev/root \
  -no-reboot \
  -usbdevice tablet

trap 'rm -r "$ROOTDIR"' INT TERM EXIT
