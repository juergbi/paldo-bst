include: base.inc

kind: autotools

build-depends:
  (>):
  - autoconf.bst
  - automake.bst
  - gettext.bst
  - libtool.bst
  - which.bst
